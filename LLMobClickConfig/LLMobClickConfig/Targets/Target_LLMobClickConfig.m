//
//  Target_LLMobClickConfig.m
//  LLMobClickConfig
//
//  Created by muyear on 2018/12/12.
//  Copyright © 2018 muyear. All rights reserved.
//

#import "Target_LLMobClickConfig.h"
#import <UMMobClick/MobClick.h>
@implementation Target_LLMobClickConfig
- (void)Action_startWithAppKey:(NSDictionary *)params {
    NSString *bundleVersion = params[@"bundleVersion"];
    NSString *appKey = params[@"appKey"];
    NSString *channelId = params[@"channelId"];
    [MobClick setAppVersion:bundleVersion];
    
    UMConfigInstance.appKey = appKey;
    UMConfigInstance.channelId = channelId;
    UMConfigInstance.ePolicy = REALTIME;
    [MobClick startWithConfigure:UMConfigInstance];

    [MobClick setEncryptEnabled:NO];
    [MobClick setCrashReportEnabled:NO];
    
}

//MARK:点击登录
- (void)Action_eventLoginWithSource:(NSDictionary *)params {
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:获取手机验证码
- (void)Action_eventLoginCaptha:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:登录结果
- (void)Action_eventLoginResult:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击查看隐私
- (void)Action_eventClickPrivacy:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击tag列表
- (void)Action_eventClickTags:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:浏览热卖会场
- (void)Action_eventReviewHot:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:浏览预告会场
- (void)Action_eventReviewPresale:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击客服
- (void)Action_eventClickedService:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击会场列表
- (void)Action_eventClickedActivityList:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:浏览商品详情
- (void)Action_eventReviewProductDetail:(NSDictionary *)params
{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:立即抢购
- (void)Action_eventClickedBuy:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:选择sku
- (void)Action_eventClickedSku:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:付款
- (void)Action_eventClickedPay:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:浏览秒杀订单
- (void)Action_eventReviewOrder:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:订单详情
- (void)Action_eventReviewOrderDetail:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:浏览分类主页面
- (void)Action_eventReviewCategory:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:浏览我的页面
- (void)Action_eventReviewMinePage:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击我的头衔
- (void)Action_eventClickedHead:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:更换头像
- (void)Action_eventClickedChangeHead:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:更改昵称
- (void)Action_eventClickedNickName:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击收获地址
- (void)Action_eventClickedAddress:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:新增收获地址
- (void)Action_eventClickedAddAddress:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:删除收获地址
- (void)Action_eventClickedDelAddress:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:编辑收获地址
- (void)Action_eventClickedEditeAddress:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:推送设置
- (void)Action_eventClickedNotiSetting:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击浏览记录
- (void)Action_eventClickedReviewHistory:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击清除缓存
- (void)Action_eventClickedClearChache:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击退出登陆
- (void)Action_eventClickedLogOut:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击切换语言
- (void)Action_eventClickedSwitchLanguage:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击设置
- (void)Action_eventClickedSetting:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:点击搜索框
- (void)Action_eventClickedSearchTF:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:取消搜索
- (void)Action_eventClickedCancelSearch:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}

//MARK:搜索商品
- (void)Action_eventClickedSearchProduct:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:进入分类
- (void)Action_eventClickedCategory:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:进入分类详情
- (void)Action_eventClickedCategoryDetail:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:进入首页
- (void)Action_eventClickedHome:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:点击首页banner
- (void)Action_eventClickedHomeBanner:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:点击首页优品
- (void)Action_eventClickedHomeYoupin:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:点击首页频道
- (void)Action_eventClickedChannel:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:漏洞福利社
- (void)Action_eventClickedWelfare:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:漏洞福利单
- (void)Action_eventClickedWelfareDetail:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:购物车
- (void)Action_eventClickedShoppingCart:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:淘宝授权登录
- (void)Action_eventClickedTaoBaoLogin:(NSDictionary *)params {
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:点击淘宝商品搜索bar
- (void)Action_eventClickedSearchBar:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:淘宝商品搜索
- (void)Action_eventClickedSearchTBProduct:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:淘宝商品详情
- (void)Action_eventClickedTBProductDetail:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:立即领券
- (void)Action_eventClickedGetCoupon:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:复制淘口令
- (void)Action_eventClickedCopyTKL:(NSDictionary *)params{
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:淘宝订单
- (void)Action_eventClickedTaobaoOrder:(NSDictionary *)params {
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:关于好省
- (void)Action_eventClickedAboutHaosheng:(NSDictionary *)params {
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
//MARK:售后订单
- (void)Action_eventClickedServerOrder:(NSDictionary *)params {
    [MobClick event:params[@"event"] attributes:@{@"attribute":[self convent_jsonStringWithParams:params]}];
}
- (NSString*)convent_jsonStringWithParams:(NSDictionary *)params {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0
                                                         error:&error];
   return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

@end

