//
//  Target_LLMobClickConfig.h
//  LLMobClickConfig
//
//  Created by muyear on 2018/12/12.
//  Copyright © 2018 muyear. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Target_LLMobClickConfig : NSObject
- (void)Action_startWithAppKey:(NSDictionary *)params;
//MARK:点击登录
- (void)Action_eventLoginWithSource:(NSDictionary *)params;

//MARK:获取手机验证码
- (void)Action_eventLoginCaptha:(NSDictionary *)params;

//MARK:登录结果
- (void)Action_eventLoginResult:(NSDictionary *)params;

//MARK:点击查看隐私
- (void)Action_eventClickPrivacy:(NSDictionary *)params;

//MARK:点击tag列表
- (void)Action_eventClickTags:(NSDictionary *)params;

//MARK:浏览热卖会场
- (void)Action_eventReviewHot:(NSDictionary *)params;

//MARK:浏览预告会场
- (void)Action_eventReviewPresale:(NSDictionary *)params;

//MARK:点击客服
- (void)Action_eventClickedService:(NSDictionary *)params;

//MARK:点击会场列表
- (void)Action_eventClickedActivityList:(NSDictionary *)params;
//MARK:浏览商品详情
- (void)Action_eventReviewProductDetail:(NSDictionary *)params;

//MARK:立即抢购
- (void)Action_eventClickedBuy:(NSDictionary *)params;

//MARK:选择sku
- (void)Action_eventClickedSku:(NSDictionary *)params;

//MARK:付款
- (void)Action_eventClickedPay:(NSDictionary *)params;

//MARK:浏览秒杀订单
- (void)Action_eventReviewOrder:(NSDictionary *)params;

//MARK:订单详情
- (void)Action_eventReviewOrderDetail:(NSDictionary *)params;

//MARK:浏览分类主页面
- (void)Action_eventReviewCategory:(NSDictionary *)params;

//MARK:浏览我的页面
- (void)Action_eventReviewMinePage:(NSDictionary *)params;

//MARK:点击我的头衔
- (void)Action_eventClickedHead:(NSDictionary *)params;

//MARK:更换头像
- (void)Action_eventClickedChangeHead:(NSDictionary *)params;

//MARK:更换头像
- (void)Action_eventClickedNickName:(NSDictionary *)params;

//MARK:点击收获地址
- (void)Action_eventClickedAddress:(NSDictionary *)params;

//MARK:新增收获地址
- (void)Action_eventClickedAddAddress:(NSDictionary *)params;

//MARK:删除收获地址
- (void)Action_eventClickedDelAddress:(NSDictionary *)params;

//MARK:编辑收获地址
- (void)Action_eventClickedEditeAddress:(NSDictionary *)params;

//MARK:推送设置
- (void)Action_eventClickedNotiSetting:(NSDictionary *)params;

//MARK:点击浏览记录
- (void)Action_eventClickedReviewHistory:(NSDictionary *)params;

//MARK:点击清除缓存
- (void)Action_eventClickedClearChache:(NSDictionary *)params;

//MARK:点击退出登陆
- (void)Action_eventClickedLogOut:(NSDictionary *)params;

//MARK:点击切换语言
- (void)Action_eventClickedSwitchLanguage:(NSDictionary *)params;

//MARK:点击设置
- (void)Action_eventClickedSetting:(NSDictionary *)params;

//MARK:点击搜索框
- (void)Action_eventClickedSearchTF:(NSDictionary *)params;

//MARK:取消搜索
- (void)Action_eventClickedCancelSearch:(NSDictionary *)params;

//MARK:搜索商品
- (void)Action_eventClickedSearchProduct:(NSDictionary *)params;

//MARK:进入分类
- (void)Action_eventClickedCategory:(NSDictionary *)params;
//MARK:进入分类详情
- (void)Action_eventClickedCategoryDetail:(NSDictionary *)params;
//MARK:进入首页
- (void)Action_eventClickedHome:(NSDictionary *)params;
//MARK:点击首页banner
- (void)Action_eventClickedHomeBanner:(NSDictionary *)params;
//MARK:点击首页优品
- (void)Action_eventClickedHomeYoupin:(NSDictionary *)params;
//MARK:点击首页频道
- (void)Action_eventClickedChannel:(NSDictionary *)params;
//MARK:漏洞福利社
- (void)Action_eventClickedWelfare:(NSDictionary *)params;
//MARK:漏洞福利单
- (void)Action_eventClickedWelfareDetail:(NSDictionary *)params;
//MARK:购物车
- (void)Action_eventClickedShoppingCart:(NSDictionary *)params;
//MARK:淘宝授权登录
- (void)Action_eventClickedTaoBaoLogin:(NSDictionary *)params;
//MARK:点击淘宝商品搜索bar
- (void)Action_eventClickedSearchBar:(NSDictionary *)params;
//MARK:淘宝商品搜索
- (void)Action_eventClickedSearchTBProduct:(NSDictionary *)params;
//MARK:淘宝商品详情
- (void)Action_eventClickedTBProductDetail:(NSDictionary *)params;
//MARK:立即领券
- (void)Action_eventClickedGetCoupon:(NSDictionary *)params;
//MARK:复制淘口令
- (void)Action_eventClickedCopyTKL:(NSDictionary *)params;
//MARK:淘宝订单
- (void)Action_eventClickedTaobaoOrder:(NSDictionary *)params;
//MARK:关于好省
- (void)Action_eventClickedAboutHaosheng:(NSDictionary *)params;
//MARK:售后订单
- (void)Action_eventClickedServerOrder:(NSDictionary *)params;
@end

NS_ASSUME_NONNULL_END

